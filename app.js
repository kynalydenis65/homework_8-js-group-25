//1.Опишіть своїми словами що таке Document Object Model (DOM)
// Document Object Model(DOM) - це представлення структури і вмісту веб -
//сторінки в об'єктному вигляді, яке дозволяє програмам і скриптам взаємодіяти з веб-сторінкою.

//2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// різниця полягає в тому, що innerHTML працює з HTML - розміткою, включаючи її виконання,
//     тоді як innerText працює лише з текстом і ігнорує HTML - розмітку.
// Вибір між ними залежить від того, які завдання ви хочете виконати з текстовим вмістом елементів.

//3.Як можна звернутися до елемента сторінки за допомогою JS ? Який спосіб кращий ?
// const h1 = document.getElementById("title");
// console.log(h1)
// const h1 = document.getElementsByClassName("title")
// console.log(h1)
// const h1 = document.getElementsByTagName("title")
// console.log(h1)

//Ці способи для мене кращі
// getElementById, getElementsByClassName, querySelector, querySelectorAll.




const paragraphs = document.querySelectorAll('p');
for (let i = 0; i < paragraphs.length; i++) {
    paragraphs[i].style.backgroundColor = '#ff0000';
}

const optionsList = document.getElementById('optionsList');
console.log('Елемент з id "optionsList":', optionsList);

const parentElement = optionsList.parentNode;
console.log('Батьківський елемент:', parentElement);

const childNodes = optionsList.childNodes;
for (const node of childNodes) {
    console.log('Назва:', node.nodeName, 'Тип:', node.nodeType);
}

const testParagraph = document.getElementById('testParagraph');
testParagraph.textContent = 'This is a paragraph';

const elementsWithClass = document.getElementsByClassName('testParagraph');

const mainHeader = document.querySelector('.main-header');
const childElements = mainHeader.children;

for (const element of childElements) {
    console.log(element);
    element.classList.add('nav-item');
}

const elementsWithClass1 = document.querySelectorAll('.section-title');
elementsWithClass1.forEach(element => {
    element.classList.remove('section-title');
});
